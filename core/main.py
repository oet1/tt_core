from fastapi import FastAPI
from core.enum import ResponseStatusEnum
app = FastAPI()

@app.get("/", status_code=200)
async def root():
    return {
            "status": ResponseStatusEnum.OK.value,
            "message": "Hello World"
            }