from core.database import Base
from sqlalchemy import Column
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy import String
import uuid


class User(Base):
    """Example User Model with SqlAlchemy
    First import from database.py declarative base to
    extend model class. So the table automatically added
    to db with alembic tool.
    Read alembic autogenerate, postgresql UUID type,
    SqlAlchemy declarative_base
    """
    __tablename__ = "user"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    username = Column(String(256), unique=True, nullable=False)
    password = Column(String(256), nullable=False)

# class User:
#     def __init__(self):
#         self.id = id
#         self.username =  username
#         self.password = password
#         self.email_address = email_address
#
#
#     @property
#     def username(self):
#         return self.username
#
#     @username.setter
#     def username(self, new_username):
#         self.username = new_username
#
#     @property
#     def password(self):
#         return self.password
#
#     @password.setter
#     def password(self, new_password):
#         self.password = new_password
#
#     @property
#     def email_address(self):
#         return self.email_address
#
#     @email_address.setter
#         def email_address(self, new_email_address):
#         self.email_address = new_email_address
#
#
# class tips:
#     def __init__(self):
#         self.id = id
#         self.name = name
#         self.description = description
#
# class tags(tips):
#     self.name = name
#     self.tag_pic_id = tag_pic_id

