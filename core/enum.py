from enum import Enum


class ResponseStatusEnum(Enum):
    OK = "OK"
    FAIL = "FAIL"