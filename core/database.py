from dotenv import load_dotenv
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import Session
import os

load_dotenv()

DB_CONNECTION_STR = f"postgresql+psycopg2://{os.getenv('RDS_DB_USERNAME')}:{os.getenv('RDS_DB_PASSWORD')}@{os.getenv('RDS_ENDPOINT')}:{os.getenv('RDS_PORT')}/{os.getenv('RDS_DB_NAME')}"
print(DB_CONNECTION_STR)

Base = declarative_base()
engine = create_engine(DB_CONNECTION_STR, echo=True)
Base.metadata.create_all(engine)
orm_session = Session(engine)