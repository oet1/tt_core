from starlette.testclient import TestClient
from core.main import app
from core.enum import ResponseStatusEnum

client = TestClient(app)

def test_root():
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() ==  {
            "status": ResponseStatusEnum.OK.value,
            "message": "Hello World"
            }